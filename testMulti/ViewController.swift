//
//  ViewController.swift
//  testMulti
//
//  Created by Konstantin on 14.05.16.
//  Copyright © 2016 Konstantin. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {

    var bird = UIView()
    var audioPlayer = AVAudioPlayer()
    
    @IBOutlet var dog: UIView!
    @IBOutlet var sky: UIView!
    @IBOutlet var ocean: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        addBird()
        parallaxForView(dog, withRange: 40)
        parallaxForView(ocean, withRange: 20)
        playSound()
    }

    func addBird() {
        
        bird.frame = CGRectMake(75, 60, 80, 60)
        bird.backgroundColor = UIColor.blueColor()
        bird.alpha = 0.2
        
        self.view.addSubview(bird)
    }
    
    func playSound() {
        
        if let soundURL = NSBundle.mainBundle().URLForResource("01", withExtension: "mp3") {
            audioPlayer = try! AVAudioPlayer(contentsOfURL: soundURL)
            audioPlayer.volume = 0.7
            audioPlayer.play()
        }
    }
    
    func parallaxForView(view: UIView!, withRange range: Int) {
        
        // Set vertical effect
        let verticalMotionEffect = UIInterpolatingMotionEffect(keyPath: "center.y", type: .TiltAlongVerticalAxis)
        verticalMotionEffect.minimumRelativeValue = -range
        verticalMotionEffect.maximumRelativeValue = range
        
        // Set horizontal effect
        let horizontalMotionEffect = UIInterpolatingMotionEffect(keyPath: "center.x", type: .TiltAlongHorizontalAxis)
        horizontalMotionEffect.minimumRelativeValue = -range
        horizontalMotionEffect.maximumRelativeValue = range
        
        // Create group to combine both
        let group = UIMotionEffectGroup()
        group.motionEffects = [horizontalMotionEffect, verticalMotionEffect]
        
        // Add both effects to your view
        view.addMotionEffect(group)
    }
    
}

